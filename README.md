# ENS Rennes Beamer theme

This is a simple Beamer theme for [ENS Rennes](https://www.ens-rennes.fr) related
presentations.

To use this theme, please install texlive 2017. For ubuntu users, go to tug.org and download [this file](http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz)
